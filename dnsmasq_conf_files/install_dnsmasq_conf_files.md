```bash
sudo apt-get install -y dnsmasq
sudo cp /etc/dnsmasq.conf  /etc/dnsmasq.conf.original

sudo vi /etc/dnsmasq-hosts.conf
<< 'content-/etc/dnsmasq-hosts.conf'
192.168.7.1 SERVER-MIAMGATEAU-1
192.168.2.1 PC-MIAMGATEAU-SUPERVISION-1
192.168.3.1 PC-MIAMGATEAU-ADMINISTRATIF-1
content-/etc/dnsmasq-hosts.conf

sudo vi /etc/dnsmasq-dns.conf
<< 'content-/etc/dnsmasq-dns.conf'
nameserver 8.8.8.8
nameserver 1.1.1.1
content-/etc/dnsmasq-dns.conf

sudo vi /etc/dnsmasq.conf
<< 'content-/etc/dnsmasq.conf'
#### DNS ####
domain-needed
bogus-priv
# Ficher des forwarders pour faire du recursive
resolv-file=/etc/dnsmasq-dns.conf
strict-order
# Fichier des enregistrements A et AAAA pour faire l'authoritative
addn-hosts=/etc/dnsmasq-hosts.conf
expand-hosts
domain=miamgateau.lan
# LOG DNS
log-queries
no-negcache
interface=enp0s3
content-/etc/dnsmasq.conf

sudo systemctl enable dnsmasq
sudo systemctl restart dnsmasq
sudo apt install firewalld
firewall-cmd --add-service=dns --permanent
firewall-cmd --reload 
```
